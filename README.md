# soal-shift-sisop-modul-4-ITB11-2022 

Penjelasan dan penyelesaian Soal Shift 4 Sistem Operasi 2021 Kelompok ITB11

1. Damarhafni Rahmannabel Nadim P (5027201026)
2. Achmad Aushaf Amrega Hisyam (5027201036)

Minal Aidzin Wal Faidzin.
Mohon maaf lahir dan batin.

![](img/img.jpg)

# Daftar Isi

* [Daftar Isi](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#daftar-isi)
* [Soal 1](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-1)
    - [Soal 1.a.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-1a)
    - [Soal 1.b.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-1b)
    - [Soal 1.c.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-1c)
    - [Soal 1.d.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-1d)
    - [Soal 1.e.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-1e)
    - [Dokumentasi Soal 1](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#dokumentasi-soal-1)
* [Soal 2](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-2)
    - [Soal 2.a.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-2a)
    - [Soal 2.b.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-2b)
    - [Soal 2.c.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-2c)
    - [Soal 2.d.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-2d)
    - [Soal 2.e.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-2e)
    - [Soal 2.f.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-2f)
    - [Soal 2.g.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-2g)
    - [Dokumentasi Soal 2](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#dokumentasi-soal-2)
* [Soal 3](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-3)
    - [Soal 3.a.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-3a)
    - [Soal 3.b.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-3b)
    - [Soal 3.c.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022/#soal-3c)
    - [Soal 3.d.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022/#soal-3d)
    - [Soal 3.e.](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#soal-3e)
    - [Dokumentasi Soal 3](https://gitlab.com/Amrega/soal-shift-sisop-modul-4-ITB11-2022#dokumentasi-soal-3)

# Soal 1
Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:
* a. Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13
	
	Contoh : 
	“Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”
* b. Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.
* c. Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.
* d. Setiap data yang terencode akan masuk dalam file “Wibu.log” 
Contoh isi: 
RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat 
RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba
* e. Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

Note : filesystem berfungsi normal layaknya linux pada umumnya, Mount source (root) filesystem adalah directory /home/[USER]/Documents, dalam penamaan file ‘/’ diabaikan, dan ekstensi tidak perlu di-encode

Referensi : https://www.base64encode.org/ https://rot13.com/

### Kendala
Kekurangan waktu untuk memahami soal dan mengerjakan soal karena bertabrakan dengan lebaran

# Soal 2
Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut : 
* a. Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere).
* b. Jika suatu direktori di rename dengan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.
* c. Apabila nama direktori dihilangkan “IAN_”, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.
* d. Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori “/home/[user]/hayolongapain_[kelompok].log”. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem.
* e. Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level INFO dan WARNING. Untuk log level WARNING, digunakan untuk mencatat syscall rmdir dan unlink. Sisanya, akan dicatat pada level INFO dengan format sebagai berikut : 

[Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC]


Keterangan : 
Level : Level logging
dd : Tanggal
mm : Bulan
yyyy : Tahun
HH : Jam (dengan format 24 Jam)
MM : Menit
SS : Detik
CMD : System call yang terpanggil
DESC : Informasi dan parameter tambahan 


Contoh : 
WARNING::21042022-12:02:42::RMDIR::/RichBrian 

INFO::21042022-12:01:20::RENAME::/Song::/IAN_Song
INFO::21042022-12:06:41::MKDIR::/BillieEilish

NOTE : 

Misalkan terdapat file bernama idontwannabeyouanymore.txt pada direktori BillieEilish. 

“IAN_Song/BillieEilish/idontwannabeyouanymore.txt” → “IAN_Song/JvyfoeRbpvyp/qqbhzwngrnhmlbognlfsek.bkg”

Referensi : https://www.dcode.fr/vigenere-cipher

### Kendala
Kekurangan waktu untuk memahami soal dan mengerjakan soal karena bertabrakan dengan lebaran

# Soal 3
Ishaq adalah seseorang yang terkenal di kalangan anak informatika seluruh indonesia. Ia memiliki teman yang bernama innu dan anya, lalu ishaq bertemu dengan mereka dan melihat program yang mereka berdua kerjakan  sehingga ia tidak mau kalah dengan innu untuk membantu anya dengan menambah fitur yang ada pada programnya dengan ketentuan :.
* a. Jika suatu direktori dibuat dengan awalan “nam_do-saq_”, maka direktori tersebut akan menjadi sebuah direktori spesial
* b. Jika suatu direktori di-rename dengan memberi awalan “nam_do-saq_”,maka direktori tersebut akan menjadi sebuah direktori spesial.
* c. Apabila direktori yang terenkripsi di-rename dengan menghapus “nam_do-saq_” pada bagian awal nama folder maka direktori tersebut menjadi direktori normal.
* d. Direktori spesial adalah direktori yang mengembalikan enkripsi/encoding pada direktori “Animeku_” maupun “IAN_” namun masing masing aturan mereka tetap berjalan pada direktori di dalamnya (sifat recursive “Animeku_” dan “IAN_” tetap berjalan pada subdirektori).
* e. Pada direktori spesial semua nama file (tidak termasuk ekstensi) pada fuse akan berubah menjadi uppercase insensitive dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya.

Contoh : jika pada direktori asli namanya adalah “isHaQ_KEreN.txt” maka pada fuse akan 
menjadi “ISHAQ_KEREN.txt.1670”. 1670 berasal dari biner 11010000110

### Kendala
Kekurangan waktu untuk memahami soal dan mengerjakan soal karena bertabrakan dengan lebaran
