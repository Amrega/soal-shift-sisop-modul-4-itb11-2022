#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#define maxSize 1024
char dirpaths[maxSize];
static  const  char *dirpath = "/home/kali/shift2";
FILE *fp;


char rot13(char src)
{
    
    if((src >= 97 && src <= 122) || (src >= 65 && src <= 90)){
        if(src > 109 || (src > 77 && src < 91)){
            //Characters that wrap around to the start of the alphabet
            src -= 13;
        }else{
            //Characters that can be safely incremented
            src += 13;
        }
    }
    return src;
}

char atbash (char src){
    if(!((src>=0&&src<65)||(src>90&&src<97)||(src>122&&src<=127)))
 {
   if(src>='A'&&src<='Z')
   src = 'Z'+'A'-src;
   if(src>='a'&&src<='z')
   src = 'z'+'a'-src;
 } 
    
    return src;
}

//melakukan encode/decode
char * encodee(char src[]) {
  char str[maxSize];
  strcpy(str, src);
  int i;
  for (i = 0; i < strlen(src); i++) {
	if(str[i] >= 'a' && str[i] <= 'z'){
        str[i] = rot13(str[i]);
    }else if(str[i] >= 'A' && str[i] <= 'Z'){
        str[i] = atbash(str[i]);
    }
  }

  char * res = str;
  fp = fopen("/home/kali/wibu.log", "a+");
  fprintf(fp, "%s\n", res);
  fclose(fp);

  return res;
}



static int xmp_readdir(const char * path, void * buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info * fi) {
  
  char fpath[maxSize];
  int* toMirror = (int*)strstr(path, "/Animeku_");
  //strcpy(fpath, find_path(path));
    strcpy(fpath, dirpath);
    strcat(fpath, path);
    printf("%s %s\n", fpath, path);
  int res = 0;
  DIR * dp;
  struct dirent * de;

  (void) offset;
  (void) fi;

  dp = opendir(fpath);
  if (dp == NULL)
	return -errno;

  while ((de = readdir(dp)) != NULL) {
	struct stat st;
	memset( & st, 0, sizeof(st));
	st.st_ino = de -> d_ino;
	st.st_mode = de -> d_type << 12;

	if (strcmp(de -> d_name, ".") == 0 || strcmp(de -> d_name, "..") == 0) {
  	res = (filler(buf, de -> d_name, & st, 0));
	} else if (toMirror) {
  	if (de -> d_type & DT_DIR) {
    	char temp[maxSize];
    	strcpy(temp, de -> d_name);

    	res = (filler(buf, encodee(temp), & st, 0));
  	} else {
    	char * ext;
    	ext = strrchr(de -> d_name, '.'); 

    	char fileName[maxSize] = "";
    	if (ext) {
      	strncpy(fileName, de -> d_name, strlen(de -> d_name) - strlen(ext));
      	strcpy(fileName, encodee(fileName)); 
      	strcat(fileName, ext);
    	} else {
      	strcpy(fileName, encodee(de -> d_name));
    	}
    	res = (filler(buf, fileName, & st, 0));
  	}
	} else
  	res = (filler(buf, de -> d_name, & st, 0));

	if (res != 0)
  	break;
  }
  closedir(dp);
  return 0;
}

static int xmp_read(const char * path, char * buf, size_t size, off_t offset, struct fuse_file_info * fi) {
  char fpath[maxSize];
  //strcpy(fpath, find_path(path));
  strcpy(fpath, dirpath);
    //strcat(fpath, path);
  int fd;
  int res;

  (void) fi;
  fd = open(fpath, O_RDONLY);
  if (fd == -1)
	return -errno;

  res = pread(fd, buf, size, offset);
  if (res == -1)
	res = -errno;

  close(fd);
  return res;
}

static int xmp_getattr(const char * path, struct stat * stbuf) {
  char fpath[maxSize];
  //strcpy(fpath, find_path(path));
  strcpy(fpath, dirpath);
    //strcat(fpath, path);
    //printf("ganti\n");
    //printf("ini getattr = %s %s\n", fpath, path);
  int res;
  res = lstat(fpath, stbuf);
  if (res == -1)
	return -errno;

  return 0;
}


static int xmp_rename(const char *from, const char *to)
{
        int res;
        char path_from[100], path_to[100];

        strcpy(path_from, dirpath);
        strcat(path_from, "/");
        strcat(path_from, from);

        strcpy(path_to, dirpath);
        strcat(path_to, "/");
        strcat(path_to, to);
        /* When we have renameat2() in libc, then we can implement flags */
        res = rename(path_from, path_to);
        printf("%s | %s\n", from, to);
        printf("res=%d\n", res);
        if (res == -1)
                return -errno;
        return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename  = xmp_rename,
};



int  main(int  argc, char *argv[])
{
    umask(0);

    /*char a[3] = "aaa";
    a[0] = atbash(a[0]);
    printf("%c", a[0]);
    return 0;*/
    remove("/home/kali/wibu.log");
    return fuse_main(argc, argv, &xmp_oper, NULL);
}